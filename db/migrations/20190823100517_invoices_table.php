<?php

use Phinx\Migration\AbstractMigration;

class InvoicesTable extends AbstractMigration
{
   
    public function up()
    {
        $users = $this->table('Invoices',['id' => 'id']);
        $users->addColumn('number', 'string', ['limit' => 40])
              ->addColumn('date', 'datetime')
              ->addColumn('value', 'float')
              ->addColumn('company_name', 'string', ['limit' => 50])
              ->addColumn('company_address', 'string', ['limit' => 50])
              ->addColumn('email', 'string', ['limit' => 50])
              ->addColumn('updated', 'datetime', ['null' => true])
              ->save();
    }

    public function down()
    {

    }
}
