$(document).ready(function(){
	$.getJSON('invoices/daily', function(data){
		var change = data.quantity[29]*100/data.quantity[28]-100;
		console.log(data.quantity[29]);
		if(change>0){
			$('#change').addClass('text-success');
			$('#change').html('<i class="fa fa-long-arrow-up"></i>'+change.toFixed(0)+'%');
		}
		else if(change<0){
			$('#change').addClass('text-danger');
			$('#change').html('<i class="fa fa-long-arrow-down"></i>'+change.toFixed(0)+'%');
		}
		else{
			$('#change').addClass('text-warning');
			$('#change').html(change.toFixed(0)+'%');
		}
		
		new Chartist.Line('.ct-chart', {
		  labels: data.labels,
		  series: [
			data.quantity
		  ]
		}, {
		  fullWidth: true,
		  chartPadding: {
			right: 40
		  }, 
		axisX: {
			labelInterpolationFnc: function skipLabels(value, index) {
			  return index % 2  === 0 ? value : null;
			}
		  }
		});
	})
})