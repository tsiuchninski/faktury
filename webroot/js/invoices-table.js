$(document).ready(function() {
		$('#invoices').DataTable( {
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": "invoices/ajax",
				headers : {
				  'X-CSRF-Token': $('#_token').val(),
			    },
				"type": "POST"
			},
			"columnDefs": [
            {
					render: function ( data, type, row ) {
						return '<a href="/invoices/view/'+ row[1] +'">'+ data + '</a>';
					},
					targets: 0
            },
			{
				render: function( data,type,row){
					return '<a href="/invoices/edit/'+ row[1] +'" class="btn btn-success btn-sm"><i class="material-icons">edit</i></a>'+
					'<a href="/invoices/delete/'+ row[1] +'" class="btn btn-danger btn-sm"><i class="material-icons">delete</i></a>'
				},
				className: 'text-right',
				targets: 7
			},
			{ "visible": false,  "targets": [ 1 ] }
        ]
			
		} );
	} );