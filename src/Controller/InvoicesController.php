<?php
namespace App\Controller;


use Cake\Datasource\ConnectionManager;
use Cake\I18n\Date;

class InvoicesController extends AppController
{
	
	public function index()
    {
			 $this->loadComponent('Paginator');
			$invoices = $this->Paginator->paginate($this->Invoices->find());
			$query = array();
			$query['count']  = "SELECT count(id) AS count  FROM `invoices` Invoices WHERE 1=1";

			$query['detail'] = "SELECT id, `number`, `date`, `value`, `company_name`, `company_address`, `email` FROM `invoices` Invoices WHERE 1=1";
			$this->request->session()->write('query', $query);
		
        $this->set(compact('invoices'));
    }
	
	public function view($id = null)
	{
		$invoice = $this->Invoices->findById($id)->firstOrFail();
		$this->set(compact('invoice'));
	}
	
	public function add()
    {
        $invoice = $this->Invoices->newEntity();
        if ($this->request->is('post')) {
            $invoice = $this->Invoices->patchEntity($invoice, $this->request->getData());

            if ($this->Invoices->save($invoice)) {
                $this->Flash->success(__('Your invoice has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to add your invoice.'));
        }
        $this->set('invoice', $invoice);
    }
	
	public function edit($id)
	{
		$invoice = $this->Invoices->findById($id)->firstOrFail();
		if ($this->request->is(['post', 'put'])) {
			$this->Invoices->patchEntity($invoice, $this->request->getData());
			if ($this->Invoices->save($invoice)) {
				$this->Flash->success(__('Your invoice has been updated.'));
				return $this->redirect(['action' => 'index']);
			}
			$this->Flash->error(__('Unable to update your invoice.'));
		}

		$this->set('invoice', $invoice);
	}
	
	public function daily()
	{
		for($i=29;$i>=0;$i--){
			$query = $this->Invoices->find('all')->where(['Invoices.date >=' => new Date('-'.$i.' days'),'Invoices.date <' => new Date('-'.($i-1).' days')]);
			$data = $query->toArray();
			$quantity[]=sizeof($data);
			$dates[]= date("d.m", strtotime(new Date('-'.$i.' days')));
		}
		$result = ['quantity'=>$quantity, 'labels'=>$dates];
		
		echo json_encode($result);exit;
	}
	
	public function delete($id)
	{
		
		$invoice = $this->Invoices->findById($id)->firstOrFail();
		if ($this->Invoices->delete($invoice)) {
			$this->Flash->success(__('The {0} invoice has been deleted.', $invoice->title));
			return $this->redirect(['action' => 'index']);
		}
	}
	
	

	public function ajax(){
        $this->autoRender = false;
        $requestData= $this->request->data;
		        extract($this->request->session()->read('query'));

		$cond = "";
        if( isset($requestData['search']['value']) && !empty( $requestData['search']['value'] ) ){
            $search = $requestData['search']['value'];
            $cond.=" AND ( Invoices.number LIKE '".$search."%' 
					OR Invoices.company_name LIKE '".$search."%' 
					OR Invoices.date LIKE '".$search."%' 
					OR Invoices.company_address LIKE '".$search."%'
					OR Invoices.value LIKE '".$search."%'
					OR Invoices.email LIKE '".$search."%')";
        }
		
		 $columns = array(
            0 => 'Invoices.number',
            1 => 'Invoices.id',
            2 => 'Invoices.date',
            3 => 'Invoices.value',
            4 => 'Invoices.company_name',
            5 => 'Invoices.company_address',
            6 => 'Invoices.email',
        );
		
		$count = $count.$cond;
        $detail = $detail.$cond;

        $conn = ConnectionManager::get('default');
        $results = $conn->execute($count)->fetchAll('assoc');
        $totalData = isset($results[0]['count']) ? $results[0]['count'] : 0;

        $totalFiltered = $totalData;

        $sidx = $columns[$requestData['order'][0]['column']];
        $sord = $requestData['order'][0]['dir'];
        $start = $requestData['start'];
        $length = $requestData['length'];

        $SQL = $detail." ORDER BY $sidx $sord LIMIT $start , $length ";
        $results = $conn->execute( $SQL )->fetchAll('assoc');

        $i = 0;
        $data = array();
        foreach ( $results as $row){
            $nestedData= [];
            $nestedData[] = $row["number"];
            $nestedData[] = $row["id"];
            $nestedData[] = date('Y-m-d', strtotime($row["date"]));
            $nestedData[] = $row["value"];
            $nestedData[] = $row["company_name"];
            $nestedData[] = $row["company_address"];
            $nestedData[] = $row["email"];
            $nestedData[] = "";
            $data[] = $nestedData;
            $i++;
        }
        $json_data = array(
            "draw"            => intval( $requestData['draw'] ),
            "recordsTotal"    => intval( $totalData ),
            "recordsFiltered" => intval( $totalFiltered ),
            "data"            => $data
        );
		
		echo json_encode($json_data);exit;
    
	}
}