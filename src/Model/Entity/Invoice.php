<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Invoice extends Entity
{
	public function beforeSave($event, $entity, $options) {
		$entity->date = date('Y-m-d', strtotime($entity->date));
	}
	
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}