<?php
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Http\Exception\NotFoundException;

$this->layout = false;

if (!Configure::read('debug')) :
    throw new NotFoundException(
        'Please replace src/Template/Pages/home.ctp with your own version or re-enable debug mode.'
    );
endif;

$cakeDescription = 'CakePHP: the rapid development PHP framework';
?>
<!doctype html>
<html lang="en">
  <head>
    <title>Faktury</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <!-- Material Dashboard CSS -->
    <?= $this->Html->css('material-dashboard.css') ?>
	
	<?= $this->Html->script(['core/jquery.min','core/popper.min','core/bootstrap-material-design.min']); ?>
  </head>
  <body>
  <nav class="navbar navbar-expand-lg bg-primary">
  <div class="container">
    <a class="navbar-brand" href="#">MENU</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-bar navbar-kebab"></span>
    <span class="navbar-toggler-bar navbar-kebab"></span>
    <span class="navbar-toggler-bar navbar-kebab"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item" <?php echo (!empty($this->request->params['controller']) && ($this->request->params['controller']=='Invoices') )?'active' :'inactive' ?>>
          <a class="nav-link" href="/">Dashboard </a>
        </li>
        <li class="nav-item <?php echo (!empty($this->request->params['controller']) && ($this->request->params['controller']=='Invoices') )?'active' :'inactive' ?>">
          <a class="nav-link" href="/invoices">Faktury <span class="sr-only">(current)</span></a>
		</li>
        <li class="nav-item">
          <a class="nav-link" href="#">Ustawienia</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
        <?= $this->fetch('content') ?>
<!--   Core JS Files   -->
  
  
  <?= $this->Html->script(['https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js']); ?>
  <?= $this->Html->script(['https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js']); ?>
  <?= $this->Html->script(['plugins/chartist.min']); ?>

 

  </body>
</html>
