<div class='container'>
<h2>Edytuj Fakturę</h2>
<?php
	

    echo $this->Form->create($invoice);
	
    echo $this->Form->create($invoice);
	
    echo $this->Form->control('number', [
		'label' => 'Numer',
		'class' => 'form-control'
	]);
    echo $this->Form->control('date',[
		'label' => 'Data',
		'type' => 'text',
		'value' => date('Y-m-d', strtotime($invoice->date)),
		'class'=>'form-control'
	]);
	echo $this->Form->control('value',[
		'label' => 'Kwota',
		'class' => 'form-control'
	]);
	echo $this->Form->control('company_name',[
		'label' => 'Nazwa Firmy',
		'class' => 'form-control'
	]);
	echo $this->Form->control('company_address',[
		'label' => 'Adres',
		'class' => 'form-control'
	]);
	echo $this->Form->control('email',[
		'label' => 'E-mail',
		'class' => 'form-control'
	]);

    echo $this->Form->button(__('Edytuj Fakturę'), ['class'=>'btn btn-primary']);
    echo $this->Form->end();
?>
</div>