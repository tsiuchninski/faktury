<div class='container'>
<h1>Faktury</h1>
<?= 
	$this->Html->link(
		'Dodaj',
		'/invoices/add',
		['class' => 'btn btn-primary']
	); 
?>
<input type='hidden' value='<?= $this->request->getParam("_csrfToken"); ?>' id='_token'>
<table class='table' id='invoices' style='width:100%'>
    <thead>
        <tr>
            <th>Numer</th>
			<th>ID</th>
            <th>Data</th>
			<th>Kwota</th>
			<th>Nazwa Firmy</th>
			<th>Adres</th>
			<th>Email</th>
			<th class='text-right'>Akcje</th>
        </tr>
    </thead>
</table>
</div>

<?= $this->Html->script(['invoices-table']); ?>
