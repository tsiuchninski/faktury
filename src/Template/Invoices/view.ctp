<div class='container'>
<h1>Faktura</h1>
<table class='table'>
    <tr>
        <th>Numer</th>
        <td>
            <?= $invoice->number ?>
        </td>
    </tr>
    
    <tr>
        <th>Data</th>
        <td>
            <?= $invoice->date->format('Y-m-d H:i') ?>
        </td>
    </tr>
	
	<tr>
        <th>Kwota</th>
        <td>
            <?= $invoice->value ?>
        </td>
    </tr>
	
	<tr>
        <th>Nazwa Firmy</th>
        <td>
            <?= $invoice->company_name ?>
        </td>
    </tr>
	
	<tr>
        <th>Adres</th>
        <td>
            <?= $invoice->company_address ?>
        </td>
    </tr>
	
	<tr>
        <th>e-mail</th>
        <td>
            <?= $invoice->email ?>
        </td>
    </tr>
	
</table>