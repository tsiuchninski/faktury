<div class='container'>
    <h2>Dashboard</h2>
	<div class='row'>
    <div class='col-md-12'>
      <div class='card'>
        <div class='card-header card-chart card-header-info'>
          <div class='ct-chart' id='invoicesChart'></div>
        </div>
        <div class='card-body'>
          <h4 class='card-title'>Faktury/Dzień</h4>
          <p class='card-category'><span id='change'><i class='fa fa-long-arrow-up'></i>   </span> Zmiana ilości faktur w dniu dzisiejszym.</p>
        </div>
        <div class='card-footer'>
          <div class='stats'>
            <i class='material-icons'>access_time</i> <?= date('Y-m-d, H:i') ?>
          </div>
        </div>
      </div>
    </div>
   
  </div>
</div>
<?= $this->Html->script(['invoices-chart']); ?>